# Copyright 2014 The University of Edinburgh
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin
from django.contrib.auth.models import Group
from django.db import models
from django.forms import TextInput, Textarea
from guardian.admin import GuardedModelAdmin
from reversion.admin import VersionAdmin
from watson.admin import SearchAdmin

from vercereg.models import Connection
from vercereg.models import FnImplementation
from vercereg.models import FunctionSig
from vercereg.models import LiteralSig
from vercereg.models import PEImplementation
from vercereg.models import PESig
from vercereg.models import RegistryUserGroup
from vercereg.models import Workspace


class PEImplInLine(admin.StackedInline):
    model = PEImplementation
    extra = 1


class FnImplInLine(admin.StackedInline):
    model = FnImplementation
    extra = 1


class ConnectionInLine(admin.TabularInline):
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '20'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 1, 'cols': 30})},
    }
    model = Connection
    extra = 1


class PESigAdmin(VersionAdmin, GuardedModelAdmin, SearchAdmin):
    list_display = ('workspace', 'pckg', 'name', 'user')
    inlines = [ConnectionInLine, PEImplInLine, ]
    search_fields = ('description', 'name',)


class LiteralSigAdmin(VersionAdmin, GuardedModelAdmin, SearchAdmin):
    list_display = ('workspace', 'pckg', 'name', 'user', 'description', 'value')
    search_fields = ('name', 'description', 'value')


class FunctionSigAdmin(VersionAdmin, GuardedModelAdmin, SearchAdmin):
    list_display = ('workspace', 'pckg', 'name', 'user')
    inlines = [FnImplInLine, ]
    search_fields = ('description', 'name',)


class PEImplementationAdmin(VersionAdmin, admin.ModelAdmin):
    list_display = ('parent_sig', 'description', 'short_code')


class FnImplementationAdmin(VersionAdmin, admin.ModelAdmin):
    list_display = ('parent_sig', 'description', 'short_code')


class PESigsInLine(admin.TabularInline):
    model = PESig
    extra = 1


class FunctionSigsInLine(admin.TabularInline):
    model = FunctionSig
    extra = 1


class WorkspaceAdmin(VersionAdmin,
                     GuardedModelAdmin,
                     SearchAdmin):
    list_display = ('name', 'description', 'owner',)
    search_fields = ('name', 'description',)


class RegistryUserGroupInline(admin.StackedInline):
    model = RegistryUserGroup
    can_delete = False


class GroupAdmin(GroupAdmin):
    inlines = [RegistryUserGroupInline]


admin.site.unregister(Group)
admin.site.register(Group, GroupAdmin)
admin.site.register(PESig, PESigAdmin)
admin.site.register(FunctionSig, FunctionSigAdmin)
admin.site.register(LiteralSig, LiteralSigAdmin)
admin.site.register(PEImplementation, PEImplementationAdmin)
admin.site.register(FnImplementation, FnImplementationAdmin)
admin.site.register(Workspace, WorkspaceAdmin)
