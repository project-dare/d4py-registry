# Copyright 2014 The University of Edinburgh
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import json
from os.path import join, exists, dirname, realpath
from os import environ, mkdir


def load_properties():
    dir_path = dirname(realpath(__file__))
    properties_file_path = join(dir_path, "properties.json") if exists(join(dir_path, "properties.json")) else \
        join(dir_path, "example_properties.json")
    with open(properties_file_path) as f:
        return json.loads(f.read())


all_properties = load_properties()
properties = all_properties["settings"]
SECRET_KEY = properties["SECRET_KEY"] if properties[
    "SECRET_KEY"] else "vzb127q8k@vz5mqt5ct-(20ddyaklr4kuy^65!8+az0u)a!*^s"

database_to_use = properties["use_database"]
database = properties["DATABASES"][database_to_use]

if database:
    try:
        database["HOST"] = environ["DJ_REG_DB_SERVICE_HOST"]
        database["PORT"] = environ["DJ_REG_DB_SERVICE_PORT"]
    except (KeyError, Exception):
        pass

DATABASES = {
    'default': {
        'ENGINE': database["ENGINE"],
        'NAME': database["NAME"],
        'USER': database["USER"],
        'PASSWORD': database["PASSWORD"],
        'HOST': database["HOST"],
        'PORT': database["PORT"],
        'OPTIONS': database["OPTIONS"]
    }
}

# Define auth backend
host = environ["DARE_LOGIN_PUBLIC_SERVICE_HOST"] if "DARE_LOGIN_PUBLIC_SERVICE_HOST" in environ else "localhost"
port = environ["DARE_LOGIN_PUBLIC_SERVICE_PORT"] if "DARE_LOGIN_PUBLIC_SERVICE_PORT" in environ else 5001

if host == "localhost":
    DARE_LOGIN_URL = "https://platform.dare.scai.fraunhofer.de/dare-login"
else:
    DARE_LOGIN_URL = "http://{}:{}".format(host, port)

DEBUG = properties["DEBUG"]
ALLOWED_HOSTS = properties["ALLOWED_HOSTS"]
BASE_DIR = dirname(dirname(__file__))

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'debug': DEBUG,
            'libraries': {  # Adding this section should work around the issue.
                'staticfiles': 'django.templatetags.static',
            },
        },
    },
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_extensions',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_framework_swagger',
    'reversion',
    'vercereg',
    'guardian',
    'watson',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.RemoteUserMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'dj_vercereg.urls'

WSGI_APPLICATION = 'dj_vercereg.wsgi.application'

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# STATIC_ROOT= os.path.join(BASE_DIR, 'static')
# STATICFILES_DIRS = (
#     os.path.join(BASE_DIR, 'static'),
# )

STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        # 'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',
    # 'PAGINATE_BY': 10,
}

# For django guardian: ########################################
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',  # default
    'guardian.backends.ObjectPermissionBackend',
    'vercereg.auth.DareAuthBackend'
)
ANONYMOUS_USER_ID = -1

SWAGGER_SETTINGS = {
    "exclude_namespaces": [],  # List URL namespaces to ignore
    "api_version": '0.1',  # Specify your API's version
    # "api_path": "/d4p-registry",  # Specify the path to your API not a root level
    "api_path": "/",  # Specify the path to your API not a root level
    "enabled_methods": [  # Specify which methods to enable in Swagger UI
        'get',
        'post',
        'put'
    ],
    # "api_key": '4737f71829a7eff1e077268b89696ab536c26a11', # An API key
    "is_authenticated": False,  # Set to True to enforce user authentication,
    "is_superuser": False,  # Set to True to enforce admin only access
    "permission_denied_handler": None,  # If user has no permisssion, raise 403 error
    "permission_classes": [],
    "info": {
        'contact': 'iraklis.klampanos@ed.ac.uk',
        'description': '',
        'license': 'Apache 2.0',
        'licenseUrl': 'http://www.apache.org/licenses/LICENSE-2.0.html',
        'termsOfServiceUrl': '',
        'title': 'DARE dispel4py Registry',
    },
}

if not exists(join(BASE_DIR, "logs")):
    mkdir(join(BASE_DIR, "logs"))

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(BASE_DIR, "logs", "logfile"),
            'maxBytes': 50000,
            'backupCount': 2,
            'formatter': 'standard',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'WARN',
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'workflow_registry': {
            'handlers': ['console', 'logfile'],
            'level': 'DEBUG',
        },
    }
}
