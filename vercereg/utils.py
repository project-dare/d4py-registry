# Copyright 2014 The University of Edinburgh
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import json

import requests

REST_PREFIX = ''


def get_base_rest_uri(request):
    protocol = request.is_secure() and 'https' or 'http'
    host = request.get_host()
    return protocol + '://' + host + REST_PREFIX + '/'


def extract_id_from_url(url):
    parts = url.split('/')
    return parts[-1] if parts[-1].isdigit() else parts[-2]


def split_name(fullname):
    """
    Split a pckg.name string into its package and name parts.

    :param fullname: an entity name in the form of <package>.<name>
    """
    parts = fullname.split('.')
    pkg = ".".join(parts[:-1])
    simple_name = parts[-1]
    return pkg, simple_name


def validate_token(token, full_resp=None):
    """
    Function to validate a given token. If the token is valid, the API asks for user info so as to retrieve a
    permanent id. Some of the returned fields (if token is valid) are as presented below:

    - "sub": "cfbdc618-1d1e-4b1d-9d35-3d23f271abe3",
    - "email_verified": true,
    - "name": "Sissy Themeli",
    - "preferred_username": "sthemeli",
    - "given_name": "Sissy",
    - "family_name": "Themeli",
    - "email": "sthemeli@iit.demokritos.gr"

    Args
        request: http request
        oidc: open-id client

    Returns
        str: user's permanent id
    """
    try:
        host = os.environ[
            "DARE_LOGIN_PUBLIC_SERVICE_HOST"] if "DARE_LOGIN_PUBLIC_SERVICE_HOST" in os.environ else "localhost"
        port = os.environ["DARE_LOGIN_PUBLIC_SERVICE_PORT"] if "DARE_LOGIN_PUBLIC_SERVICE_PORT" in os.environ else 5001
        url = "http://{}:{}/validate-token".format(host, port)
        data = {"access_token": token}
        if full_resp:
            data["full_resp"] = True
        response = requests.post(url, data=json.dumps(data))
        return response.status_code, response.text
    except (ConnectionError, Exception) as e:
        print(e)
        return 500, "Could not validate token"
