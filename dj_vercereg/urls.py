# Copyright 2014 The University of Edinburgh
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers
# from rest_framework.authtoken import views as tokenviews
from rest_framework_swagger.views import get_swagger_view

from vercereg import views

schema_view = get_swagger_view(title='DARE dispel4py Registry')


router = routers.DefaultRouter()
router.register(r'accounts', views.LoginView, basename="accounts")
router.register(r'users', views.UserViewSet, basename='user')
router.register(r'groups', views.GroupViewSet, basename='group')
router.register(r'registryusergroups', views.RegistryUserGroupViewSet, basename='registryusergroup')
router.register(r'workspaces', views.WorkspaceViewSet, basename='workspace')
router.register(r'pes', views.PESigViewSet, basename='pesig')
router.register(r'functions', views.FunctionSigViewSet, basename='functionsig')
router.register(r'literals', views.LiteralSigViewSet, basename='literalsig')
router.register(r'connections', views.ConnectionViewSet, basename='connection')
router.register(r'fnparams', views.FunctionParameterViewSet, basename='functionparameter')
router.register(r'peimpls', views.PEImplementationViewSet)
router.register(r'fnimpls', views.FnImplementationViewSet)

urlpatterns = [
    url(r'^docs/', schema_view),
    url(r'^admin/', admin.site.urls),
    url(r'^', include(router.urls)),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # url(r'^api-token-auth/', tokenviews.obtain_auth_token)
]
#  )

# Admin-site specifics:
admin.site.site_header = 'VERCE Registry Admin'
admin.site.site_title = "VERCE Registry"
